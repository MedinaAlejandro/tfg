# Grabar sonido mediante microfono usb
import pyaudio
import wave
import time
import sys
# Settings del audio
FORMAT= pyaudio.paInt16
CHANNELS=2
RATE=44100
CHUNK=1024
RECORD_SECONDS=5
WAVE_OUTPUT_FILENAME="grabacion.wav"
# Creamos objeto pyAudio esto genera mensajes de error que no afectan al funcionamiento
audio=pyaudio.PyAudio()
# Empieza a grabar mediante la variable stream
stream=audio.open(format=FORMAT,channels=CHANNELS,rate=RATE,input=True,frames_per_buffer=CHUNK)
print("recording")
frames=[] # Variable en la que se guardan los frames grabados
# Bucle para grabar el tiempo que hemos estimado
for i in range(0,int(RATE/CHUNK*RECORD_SECONDS)):
	data=stream.read(CHUNK)
	frames.append(data) #Se van guardando los frames dentro de la variable del mismo nombre
print("Finished recording")
# Parar de grabar
stream.stop_stream()
stream.close()
# Generamos un archivo .wav para guardar el audio
wf=wave.open(WAVE_OUTPUT_FILENAME,'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(audio.get_sample_size(FORMAT))
wf.setframerate(RATE)
wf.writeframes(b''.join(frames))
wf.close()

#  Reproducir dicho archivo
print("Now playing \n")
# Abrimos el archivo
wf=wave.open("grabacion.wav",'rb') # solo lectura ´rb´
# Esta vez stream es de salida observese output=True
"""
stream=audio.open(format=audio.get_format_from_width(wf.getsampwidth()),
				channels=wf.getnchannels(),
				rate=wf.getframerate(),
				output=True)
"""
stream=audio.open(format=FORMAT,channels=CHANNELS,rate=RATE,output=True)

# Lee los datos del archivo wav
data=wf.readframes(CHUNK)
# Reproduccion del archivo hasta que este finaliza
while len(data)>0:
	stream.write(data)
	data=wf.readframes(CHUNK)
# Cerrar stream y pyaudio
stream.stop_stream()
stream.close()
audio.terminate()
			
