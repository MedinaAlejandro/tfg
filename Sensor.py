import RPi.GPIO as GPIO
import dht11
import time
# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()
#definimos en que pin se encuentra el sensor
sensor = dht11.DHT11(pin=4)

temperatura_sensor=0
humedad_sensor=0
info=[0,0] #Vector para almacenar la temperatura y humedad el puesto [0] es temperatura y el [1] la humedad
def Leer():
	
	# Forzamos la lectura del sensor hasta dar con un resultado valido
	while True :
	
		result=sensor.read()
		
		if result.is_valid():
			
			print("Temperature: %d C" % result.temperature)
			print("Humidity: %d %%" % result.humidity)
			return [result.temperature,result.humidity]
			time.sleep(0.5)
			break
		 
encendido= True
while encendido:
	print("Pulsa 1 para concer la temperatura y humedad y 2 para salir \n")
	selector=input()
	if selector=='1':
		info=Leer()
		print("variable temp %d"%info[0])
		print("variable hume %d"%info[1])
	elif selector=='2':
		print("Bye \n")
		encendido=False
	else:
		print("Incorrecto \n")
	

