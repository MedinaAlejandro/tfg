# Cargamos las librerias/modulos necesarios
from bluetooth import *
import time
import subprocess
import sys
import RPi.GPIO as GPIO
import dht11
#testing
# Preparamos la raspberry para que pueda ser visible al smartphone y un puerto
subprocess.call(["sudo", "hciconfig", "hci0","piscan"])
subprocess.call(["sudo", "sdptool", "add","sp"])

# inicializamos el GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()
# Definimos en que pin se encuentra el sensor
sensor = dht11.DHT11(pin=4)
info=[0,0] #Vector para almacenar la temperatura y humedad el puesto [0] es temperatura y el [1] la humedad
# Definimos una funcion para realizar una lectura del sensor DHT11
def Leer():
	
	# Forzamos la lectura del sensor hasta dar con un resultado valido
	while True :
	
		result=sensor.read()
		
		if result.is_valid():
			# Al realizar una lectura escribe en pantalla los datos de temp y humedad y los devuelve
			print("Temperature: %d C" % result.temperature)
			print("Humidity: %d %%" % (100-result.humidity))
			return [result.temperature,100-result.humidity]
			time.sleep(0.5)
			break


hostaddr= '00:1A:7D:DA:71:04' # Direccion bluetooth de la raspberry

#-----------Crear server RFCOMM---------------------------
server_sock=BluetoothSocket( RFCOMM ) # Creamos un socket bluetooth
server_sock.bind(("",PORT_ANY))
server_sock.listen(1)

port = server_sock.getsockname()[1]
print("Esperando conexion RFCOMM en el canal %d" % port)

client_sock, client_info = server_sock.accept() # Nuevo socket con el cliente
server_sock.close() # Cerramos el socket que ya no vamos a usar
print("Conexion establecida con ", client_info)

try:
	while (True):
		data=client_sock.recv(1024) # recibir orden del smartphone
		if len(data)==0: break # salida del bucle
		orden=int(data) #para tratar la informarcion enviada como entero
		if orden==1: # Si se le envia un 1 se envian los datos de DHT11
			info=Leer() # Usa la funcion Leer y guarda los datos en el vector de enteros info
			client_sock.send(str(info[0])+','+str(info[1])) # Envio de los datos separados por una coma para que la App los interprete
		else:
			print("error al recibir info\n")


except IOError:
	pass
	
print("disconnected")
client_sock.close()
print("all done")








 
