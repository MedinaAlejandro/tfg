#  Server socket de audio

import pyaudio
import subprocess
import sys
import socket
import time
from bluetooth import *
import wave
# Setting de audio
chunk=1024
FORMAT= pyaudio.paInt16
CHANNELS=2
RATE=44100
WAVE_OUTPUT_FILENAME="grabacion.wav"

#  Server interno de las raspberry
server_addres="6"

# Creando socket

server_socket=socket.socket(socket.AF_UNIX,socket.SOCK_STREAM)
server_socket.bind(server_addres)
server_socket.listen(1) 
print("Esperando conexion\n")
client_socket,address=server_socket.accept()
print("conectado al cliente")
server_socket.close()

# Definimos callback

def callback(in_data,frame_count,time_info,status):
	client_socket.send(in_data)
	print("llamada a callback")
	return(in_data,pyaudio.paContinue)

# Creando pyadio

p=pyaudio.PyAudio()
stream=p.open(format=FORMAT,channels=CHANNELS,rate=RATE,input=True,stream_callback=callback)
stream.start_stream()
while stream.is_active():
	time.sleep(0.2) #tiempo para que no se solape informacion al enviarla y recibirla

# Cerramos socket abierto y detemos stream y pyaudio

client_socket.close()
stream.stop_stream()
stream.close()
p.terminate()

"""
subprocess.call(["sudo", "hciconfig", "hci0","piscan"])
subprocess.call(["sudo", "sdptool", "add","sp"])

server_sock=BluetoothSocket( RFCOMM ) # Creamos un socket bluetooth
server_sock.bind(("",PORT_ANY))
server_sock.listen(1)
print("Esperando30onexion")
client_sock, client_info = server_sock.accept() # Nuevo socket con el cliente
server_sock.close() # Cerramos el socket que ya no vamos a usar
print("Conexion establecida con ", client_info)

#Definimos callback
def callback(in_data,frame_count,time_info,status):
	frames=[]
	frames.append(in_data)
	wf=wave.open(WAVE_OUTPUT_FILENAME,'wb')
	wf.setnchannels(CHANNELS)
	wf.setsampwidth(p.get_sample_size(FORMAT))
	wf.setframerate(RATE)
	wf.writeframes(b''.join(frames))
	wf.close()
	wf=wave.open("grabacion.wav",'rb') # solo lectura ´rb´
	data=wf.readframes(chunk)
	client_sock.send(data)
	wf.close()
	print("llamada a callback")
	return(in_data,pyaudio.paContinue)
# Creando pyadio
p=pyaudio.PyAudio()
stream=p.open(format=FORMAT,channels=CHANNELS,rate=RATE,input=True,stream_callback=callback)
stream.start_stream()

while stream.is_active():
	time.sleep(0.2)
stream.stop_stream()
stream.close()
p.terminate()
client_sock.close()
"""
